package com.latam.redis.service;

import java.util.List;

import com.latam.redis.domain.Data;

public interface RedisService {

    List<Data> findByPattern();
}
