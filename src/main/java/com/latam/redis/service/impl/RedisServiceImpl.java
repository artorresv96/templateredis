package com.latam.redis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import com.latam.redis.domain.Data;
import com.latam.redis.service.RedisService;

@Service
public class RedisServiceImpl implements RedisService{

    @Autowired
    private RedisTemplate<String, Data> keyValueTemplate;
    
    private static final String REDIS_ALL = "*";
    
    @Override
    public List<Data> findByPattern() {
        return getValueOperations().multiGet(keyValueTemplate.keys(REDIS_ALL));
    }
    


    private ValueOperations<String, Data> getValueOperations() {
        return keyValueTemplate.opsForValue();
    }
}
