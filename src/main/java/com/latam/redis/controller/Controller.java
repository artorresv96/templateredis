package com.latam.redis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.latam.redis.domain.Data;
import com.latam.redis.service.RedisService;

@RestController
public class Controller {

    @Autowired
    private RedisService redisService;
    
    @GetMapping("/redis")
    public ResponseEntity<List<Data>> getReservation() throws Exception {
        return new ResponseEntity<>(redisService.findByPattern(), HttpStatus.OK);
    }
}
