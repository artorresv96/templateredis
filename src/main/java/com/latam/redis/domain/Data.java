package com.latam.redis.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Data implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4468884570552806075L;
    private String valor;
}
